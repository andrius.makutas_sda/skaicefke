class Skaicefke():



    def pliusas(self):
        self.a = int(input('Pirmas sudeties skaicius: '))
        self.b = int(input('Antras sudeties skaicius: '))
        result = self.a + self.b
        print(f'pliuso rezultatas: {result}')

    def dalyba(self):
        self.a = int(input('Iveskite skaiciu is kurio atiminesite: '))
        self.b = int(input('Iveskite skaiciu, kuri atiminesite: '))
        result = self.a / self.b
        print(f'Dalybos rezultatas: {result}')

    def daugyba(self):
        self.a = int(input('Iveskite pirma dauginamaji: '))
        self.b = int(input('Iveskite antra dauginamaji: '))
        result = self.a * self.b
        print(f'Daugybos rezultatas: {result}')

    def atimtis(self):
        self.a = int(input('Iveskite pirma skaičių: '))
        self.b = int(input('Iveskite antra skaičių: '))
        result = self.a - self.b
        print(f'Atimties rezultatas: {result}')

    def kvadratas(self):
        self.a = int(input('Iveskite norima skaiciu, kuri kelsite kvadratu: '))
        result = self.a**2
        print(f'Skaiciaus {self.a} kvadratas yra {result}')

    def root(self):
        self.a = int(input('Iveskite norima skaičių, iš kurio išimsite kvadratą: '))
        result = self.a // 2
        print(f'Saknies rezultatas: {result}')


obj = Skaicefke()
obj.pliusas()
obj.atimtis()
obj.dalyba()
obj.daugyba()
obj.kvadratas()
obj.root()
